public class Banker implements Runnable{

    Costumer costumer;

    @Override
    public void run() {
        while(true){
            try {
                costumer = Bank.serving.getCostumer();
                System.out.println("Costumer " + costumer.turn
                        + " is being served in counter " + Thread.currentThread().getName());
                Bank.seatsSem.release();
                giveService(costumer);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    void giveService(Costumer costumer) throws InterruptedException {
        Thread.sleep((long)(Math.random() * 20000));
        System.out.println("Costumer " + costumer.turn + " is done.");
        costumer.gotService.release();
    }
}
