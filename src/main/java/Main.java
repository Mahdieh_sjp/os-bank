// critical sections: terminal, waiting area, seats, counters
// n = 6				(98222065 % 10) + 1 = 6
// m = 10	            (98222049 % 10) + 1 = 10



public class Main {


    public static void main(String[] args) throws InterruptedException {

        for(int i = 0; i < Bank.N; i++){
            new Thread(new Banker(), String.valueOf(i)).start();
            System.out.println("Banker " + i + " is ready.");
        }
        System.out.println("Ready to take in costumers.");
        while (true){
            Thread.sleep((long)(Math.random() * 1000));
            System.out.println("New costumer!");
            new Thread(new Costumer()).start();
        }
    }
}
