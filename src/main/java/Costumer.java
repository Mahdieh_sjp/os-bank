import java.util.concurrent.Semaphore;

public class Costumer implements Runnable{
    int turn;
    Semaphore gotService = new Semaphore(1);   			// check if customer's task is finished or not

    public Costumer(){
        try {
            // set the binary semaphore to 1, to wait in line 26 until costumer get service
            gotService.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            Bank.entranceSem.acquire();

            Bank.terminalSem.acquire();	//only one person can use terminal
            turn = Bank.getTurn();
            Bank.terminalSem.release();		//terminal is free to use
            System.out.println("Costumer " + turn + " is in waiting area.");

            Bank.seatsSem.acquire();

            Bank.serving.sit(this);

            gotService.acquire();

            System.out.println("Costumer " + turn + " is out.");
            Bank.entranceSem.release();


        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
