import java.util.concurrent.Semaphore;

public class Bank {
    public static final int N = 6;
    public static final int M = 10;
    public static final int MAX_NUMBER_OF_COSTUMERS = 20;

    public static ServingCostumer serving = new ServingCostumer();

    private static int turn = 1;


    public static Semaphore seatsSem = new Semaphore(M, true);
    public static Semaphore terminalSem = new Semaphore(1, true);
    public static Semaphore entranceSem = new Semaphore(MAX_NUMBER_OF_COSTUMERS, true);

    public static int getTurn(){
        return turn++;
    }

}
