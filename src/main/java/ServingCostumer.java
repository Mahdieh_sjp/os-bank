import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ServingCostumer {
    private static int counter;
    private static Queue<Costumer> sittingCostumers;

    private final Lock lock;
    private final Condition notEmpty;

    public ServingCostumer() {
        counter = 0;
        sittingCostumers = new LinkedList<>();
        lock = new ReentrantLock();
        notEmpty = lock.newCondition();
    }

    public void sit(Costumer costumer){
        lock.lock();

        try {
            sittingCostumers.add(costumer);
            System.out.println("Costumer " + costumer.turn + " is sitting.");
            counter++;
            if (counter == 1)
                notEmpty.signal();
        } finally {
            lock.unlock();
        }
    }

    public Costumer getCostumer() throws InterruptedException {
        lock.lock();

        try {
            if(counter == 0){
                notEmpty.await();
            }
            Costumer costumer = sittingCostumers.poll();
            counter--;
            return costumer;
        } finally {
            lock.unlock();
        }
    }
}


